import javax.swing.*;
import java.awt.event.*;
import java.awt.*;	

public class Pr1 extends JApplet{
	JTextField t=new JTextField("Afisare optiune selectata");
	JMenuBar mb1=new JMenuBar();
	JMenu semnal=new JMenu("Semnal intrare"),
		  tip=new JMenu("Tip sistem");
	
	JMenuItem[] smnin={new JMenuItem("Impuls Dirac"),new JMenuItem("Treapta"),new JMenuItem("Sinusoidal")};
	JMenuItem[] tsis={new JMenuItem("Ordin I"),new JMenuItem("Ordin II")};
	
	JMenuBar mb2=new JMenuBar();
	
	JMenu forma=new JMenu("Forma"),
		  dimensiuni=new JMenu("Dimensiuni caracteristice");
	
	JMenuItem[] frm={new JMenuItem("Linie"),new JMenuItem("Dreptunghi"),new JMenuItem("Cerc")};
	
	JMenu dim1=new JMenu("Dimensiune 1"),
		  dim2=new JMenu("Dimensiune 2");
	
	JRadioButtonMenuItem[] bdim1={new JRadioButtonMenuItem("1",false),new JRadioButtonMenuItem("2",false),new JRadioButtonMenuItem("3",false)};
	JRadioButtonMenuItem[] bdim2={new JRadioButtonMenuItem("5",false),new JRadioButtonMenuItem("10",false),new JRadioButtonMenuItem("100",false)};
	
	JButton b=new JButton("Comuta");
	public void init(){
		for(int i=0;i<smnin.length;i++){
			smnin[i].addActionListener(al);
			semnal.add(smnin[i]);
		}
		for(int i=0;i<tsis.length;i++){
			tsis[i].addActionListener(al);
			tip.add(tsis[i]);
		}
		mb1.add(semnal);
		mb1.add(tip);
		t.setEditable(false);
		Container cp=getContentPane();
		cp.add(t,BorderLayout.CENTER);
		b.addActionListener(al);  
		cp.add(b,BorderLayout.NORTH);
		for(int i=0;i<frm.length;i++){
			frm[i].addActionListener(al);
			forma.add(frm[i]);
		}
		for(int i=0;i<bdim1.length;i++){
			bdim1[i].addActionListener(al);
			dim1.add(bdim1[i]);
		}
		for(int i=0;i<bdim1.length;i++){
			bdim2[i].addActionListener(al);
			dim2.add(bdim2[i]);
		}
		dimensiuni.add(dim1);
		dimensiuni.add(dim2);
		mb2.add(forma);
		mb2.add(dimensiuni);
		setJMenuBar(mb1);
	}
	int d1,d2;
	boolean b1=false,b2=false;
ActionListener al=new ActionListener(){
	public void actionPerformed(ActionEvent e){
		String arg=e.getActionCommand(); 
		Object source=e.getSource(); 
		if(source.equals(b)){ 
			JMenuBar m=getJMenuBar();
			setJMenuBar(m==mb1?mb2:mb1); 
			validate();} 
		else if(source instanceof JMenuItem){ 
			if(arg.equals("Impuls Dirac")){   
				t.setText("Semnal intrare Impuls Dirac");
			}
			else if(arg.equals("Treapta")){   
				t.setText("Semnal intrare Treapta");
			} 
			else if(arg.equals("Sinusoidal")){  
				t.setText("Semnal intrare Sinusoidal");
			} 
			if (source.equals(tsis[0]))  
				t.setText("Sistem "+tsis[0].getText());
			else if(source.equals(tsis[1]))  
				t.setText("Sistem "+tsis[1].getText());
			if(arg.equals("1")){
				d1=Integer.parseInt(e.getActionCommand());
				b1=true;
				bdim1[0].setSelected(true);
				bdim1[1].setSelected(false);
				bdim1[2].setSelected(false);
			}
				else if(arg.equals("2")){
					d1=Integer.parseInt(e.getActionCommand());
					bdim1[0].setSelected(false);
					bdim1[1].setSelected(true);
					bdim1[2].setSelected(false);
					b1=true;
					}
				else if(arg.equals("3")){
					d1=Integer.parseInt(e.getActionCommand());
					bdim1[0].setSelected(false);
					bdim1[1].setSelected(false);
					bdim1[2].setSelected(true);
					b1=true;}
				if(arg.equals("5")){
					d2=Integer.parseInt(e.getActionCommand());
					bdim2[0].setSelected(true);
					bdim2[1].setSelected(false);
					bdim2[2].setSelected(false);
					b2=true;}
				else if(arg.equals("10")){
					d2=Integer.parseInt(e.getActionCommand());
					bdim2[0].setSelected(false);
					bdim2[1].setSelected(true);
					bdim2[2].setSelected(false);
					b2=true;}
				else if(arg.equals("100")){
					d2=Integer.parseInt(e.getActionCommand());
					bdim2[0].setSelected(false);
					bdim2[1].setSelected(false);
					bdim2[2].setSelected(true);
					b2=true;}
			if(arg.equals("Linie")){
				if(b1==true && b2==true)t.setText("Linie de lungime: "+(d2-d1));
				else t.setText("Selecteaza dimensiunile!");
			}
			else if(arg.equals("Dreptunghi")){
				if(b1==true && b2==true)t.setText("Linie de lungime: "+(d2*d1));
				else t.setText("Selecteaza dimensiunile!");
			}
			else if(arg.equals("Cerc")){
				if(b1==true && b2==true)t.setText("Linie de lungime: "+(3.14*(d1-d2)));
				else t.setText("Selecteaza dimensiunile!");
			}
			}}}; 
public static void main(String[] args){
			     	  JApplet applet=new Pr1();
			          JFrame cadru=new JFrame("Meniu");
			          cadru.addWindowListener(new WindowAdapter(){
			          public void windowClosing(WindowEvent e){System.exit(0);}});
			          cadru.getContentPane().add(applet);
			          cadru.setSize(300,100);
			          applet.init();
			          applet.start();
			          cadru.setVisible(true);
			          }}